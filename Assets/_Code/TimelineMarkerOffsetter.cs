﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;

// this script can be used to persistently offset markers on a timeline during play
public class TimelineMarkerOffsetter : MonoBehaviour
{
    public TimelineAsset timelineAsset;

    public int[] TrackIdsToOffset;

    public float timeOffset;


    //private void Update()
    //{
    //    if(Input.GetKeyDown(KeyCode.O))
    //    {
    //        OffsetTimelineMarkers(TrackIdsToOffset, this.timeOffset);
    //    }
    //}

    public void OffsetTimelineMarkers(int[] trackIds, float timeOffset)
    {
        for(int i = 0; i < trackIds.Length; i++)
        {
            OffsetTimelineMarker(trackIds[i], timeOffset);
        }
    }

    public void OffsetTimelineMarker(int TrackId, float timeOffset)
    {
        var track = timelineAsset.GetOutputTrack(TrackId);

        var markers = track.GetMarkers();

        foreach(var marker in markers)
        {
            marker.time += timeOffset;
        }
    }
}
