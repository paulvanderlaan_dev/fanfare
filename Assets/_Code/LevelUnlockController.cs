﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Events;

public class LevelUnlockController : MonoBehaviour
{
    //public AudioSource titleAudioSource;
    //public AudioSource preTutorialAudioSource;
    //public AudioSource kipTutorialAudioSource;
    //public AudioSource mainAudioSource;
    //public AudioSource TubaAudioSource;
    //public AudioSource drumAudioSource;
    //public AudioSource gezinAudioSource;

    public UnityEvent Level0Succes;
    public UnityEvent Level0Fail;
    public UnityEvent Level1Succes;
    public UnityEvent Level1Fail;
    public UnityEvent Level2Succes;
    public UnityEvent Level2Fail;
    public UnityEvent Level3Succes;
    public UnityEvent Level3Fail;

    private void OnEnable()
    {
        MeasureController.MeasureCollectionEnded += OnMeasureCollectionEnded;
    }

    private void OnDisable()
    {
        MeasureController.MeasureCollectionEnded -= OnMeasureCollectionEnded;
    }

    public void OnMeasureCollectionEnded(int musicLevel, bool isSuccesful)
    {
        switch(musicLevel)
        {
            case 0:
                if(isSuccesful)
                {
                    Level0Succes.Invoke();
                }
                else
                {
                    Level0Fail.Invoke();
                }
                break;
            case 1:
                if(isSuccesful)
                {
                    Level1Succes.Invoke();
                }
                else
                {
                    Level1Fail.Invoke();
                }
                break;
            case 2:
                if(isSuccesful)
                {
                    Level2Succes.Invoke();
                }
                else
                {
                    Level2Fail.Invoke();
                }
                break;
            case 3:
                if(isSuccesful)
                {
                    Level3Succes.Invoke();
                }
                else
                {
                    Level3Fail.Invoke();
                }
                break;
            default:
                break;
        }
    }
}
