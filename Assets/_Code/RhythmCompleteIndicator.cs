﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RhythmCompleteIndicator : MonoBehaviour
{
    public Image[] rhythmImages;

    public float resetUIDelaySeconds = 1f;

    private void OnEnable()
    {
        MeasureController.OnMeasureEnd += OnMeasureEnd;
        MeasureController.MeasureCollectionEnded += ResetUI;
    }

    private void OnDisable()
    {
        MeasureController.OnMeasureEnd -= OnMeasureEnd;
        MeasureController.MeasureCollectionEnded -= ResetUI;
    }

    public void OnMeasureEnd(int rhythmId, bool isSuccesful)
    {
        rhythmImages[rhythmId].gameObject.SetActive(isSuccesful);
    }

    public void ResetUI(int level, bool isSuccesful)
    {
        StartCoroutine(ResetUIDelayed(resetUIDelaySeconds));
    }

    public IEnumerator ResetUIDelayed(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        foreach(var rhythmImage in rhythmImages)
        {
            rhythmImage.gameObject.SetActive(false);
        }
    }
}
