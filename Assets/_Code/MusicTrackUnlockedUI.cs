﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicTrackUnlockedUI : MonoBehaviour
{

    public Image[] trackUnlockedImageComponents; 

    private void OnEnable()
    {
        MeasureController.MeasureCollectionEnded += OnMeasureCollectionEnded;
    }

    private void OnDisable()
    {
        MeasureController.MeasureCollectionEnded -= OnMeasureCollectionEnded;
    }

    // Start is called before the first frame update
    private void OnMeasureCollectionEnded(int musicTrack, bool isSuccesful)
    {
        trackUnlockedImageComponents[musicTrack].color = isSuccesful ? Color.green : Color.red;
    }


}
