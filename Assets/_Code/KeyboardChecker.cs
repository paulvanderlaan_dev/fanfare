﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardChecker : MonoBehaviour
{
    public NoteSpawner noteSpawner;


    void Update()
    {
        if(Input.GetButtonDown("Fire1"))
        {
            noteSpawner.checkNotes(Notes.A);
        }

        if(Input.GetButtonDown("Fire2"))
        {
            noteSpawner.checkNotes(Notes.B);
        }

        if(Input.GetButtonDown("Jump"))
        {
            noteSpawner.checkNotes(Notes.Y);
        }


    }
}
