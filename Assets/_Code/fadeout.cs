﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fadeout : MonoBehaviour
{
    CanvasGroup cg;
    // Start is called before the first frame update
    void Start()
    {
        cg = transform.GetComponent<CanvasGroup>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void doFade()
    {
        StartCoroutine(FadeMe());
    }

    IEnumerator FadeMe()
    {
        while (cg.alpha > 0)
        {
            cg.alpha -= Time.deltaTime / 2;
            yield return null;
        }
        cg.interactable = false;
        yield return null;
    }

    public void doFadeIn()
    {
        StartCoroutine(FadeMeIn());
    }

    IEnumerator FadeMeIn()
    {
        while (cg.alpha < 1)
        {
            cg.alpha += Time.deltaTime / 2;
            yield return null;
        }
        cg.interactable = false;
        yield return null;
    }
}
