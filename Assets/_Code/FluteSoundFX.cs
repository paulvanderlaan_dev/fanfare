﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FluteSoundFX : MonoBehaviour
{
    public AudioSource fluteLowSource;
    public AudioSource fluteMediumSource;
    public AudioSource fluteHighSource;

    public AudioSource fluteFalseSource;
    float falseNoteMinimumPitch = 0.5f;
    float falseNoteMaximumPitch = 1.5f;

    public NoteSpawner noteSpawner;



    // Start is called before the first frame update
    void OnEnable()
    {
        noteSpawner.SuccesfulNoteHit += PlayNote;
        noteSpawner.UnsuccesfulNoteHit += PlayFalseNote;
    }

    void OnDisable()
    {
        noteSpawner.SuccesfulNoteHit -= PlayNote;
        noteSpawner.UnsuccesfulNoteHit -= PlayFalseNote;
    }


    public void PlayNote(Notes note)
    {
        //Debug.Log("playing note: " + note.ToString());
        switch(note)
        {
            case Notes.A:
                fluteLowSource.Play();
                break;
            case Notes.B:
                fluteMediumSource.Play();
                break;
            case Notes.Y:
                fluteHighSource.Play();
                break;
            default:
                break;
        }
    }

    public void PlayFalseNote(Notes note)
    {
        float newPitch = Random.Range(falseNoteMinimumPitch, falseNoteMaximumPitch);
        fluteFalseSource.pitch = newPitch;
        fluteFalseSource.Play();
    }

}
