﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasController : MonoBehaviour
{
    CanvasGroup cg;
    // Start is called before the first frame update
    void Start()
    {
        cg = transform.GetComponent<CanvasGroup>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void doFadeOut()
    {
        StartCoroutine(FadeMeOut());
    }

    IEnumerator FadeMeOut()
    {
        while (cg.alpha > 0)
        {
            cg.alpha -= Time.deltaTime / 2;
            yield return null;
        }
        cg.interactable = false;
        yield return null;
    }

    public void doFadeIn()
    {
        StartCoroutine(FadeMeIn());
    }

    IEnumerator FadeMeIn()
    {
        while (cg.alpha < 1)
        {
            cg.alpha += Time.deltaTime / 2;
            yield return null;
        }
        cg.interactable = false;
        yield return null;
    }
}
