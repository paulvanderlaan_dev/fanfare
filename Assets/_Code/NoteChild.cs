﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NoteChild : MonoBehaviour
{
    public float spawnTime;
    public Notes note;
    public Text label;
    public float destroyTime;

    public void DuplicateSelf()
    {
        var duplicateNoteChildObject = Instantiate(gameObject, transform.parent);
        duplicateNoteChildObject.transform.position = transform.position;
        var duplicateNoteChild = duplicateNoteChildObject.GetComponent<NoteChild>();
        if(duplicateNoteChild.label != null)
        {
            duplicateNoteChild.label.text = "X";
        }
        else
        {
            print("no label found on note");
        }

        duplicateNoteChildObject.transform.SetParent(duplicateNoteChildObject.transform.parent.parent);
    }
}
