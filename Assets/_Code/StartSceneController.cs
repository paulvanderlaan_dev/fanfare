﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartSceneController : MonoBehaviour
{
    public string startButtonName = "Fire1";
    public string sceneToLoad;

    public Image backgroundImage;

    public float fadeDuration = 2f;


    private void Awake()
    {
        StartCoroutine( FadeIn());
    }

    private void Update()
    {
        if(Input.GetButtonDown(startButtonName) && Time.timeSinceLevelLoad > fadeDuration)
        {
            StartCoroutine(StartGame());
        }
    }

    public IEnumerator StartGame()
    {
        Color startColor = new Color(1f, 1f, 1f, 1f);
        Color endColor = new Color(1f, 1f, 1f, 0f);

        backgroundImage.color = startColor;

        float t = 0f;

        while(t < fadeDuration)
        {
            t += Time.deltaTime;
            float normalizedTime = t / fadeDuration;
            backgroundImage.color = Color.Lerp(startColor, endColor, normalizedTime);
            yield return null;
        }

        SceneManager.LoadScene(sceneToLoad);
    }

    public IEnumerator FadeIn()
    {
        Color startColor = new Color(1f, 1f, 1f, 0f);
        Color endColor = new Color(1f, 1f, 1f, 1f);

        backgroundImage.color = startColor;

        float t = 0f;

        while(t < fadeDuration)
        {
            t += Time.deltaTime;
            float normalizedTime = t / fadeDuration;
            backgroundImage.color = Color.Lerp(startColor, endColor, normalizedTime);
            yield return null;
        }
    }
}
