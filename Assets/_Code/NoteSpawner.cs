﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Notes {A,B,Y, MeasureBar}

public class NoteSpawner : MonoBehaviour
{

    public NoteChild noteAPrefab;
    public NoteChild noteBPrefab;
    public NoteChild noteYPrefab;
    public NoteChild measureBarPrefab;
    public Transform measureBarsParent;

    public Transform noteASpawner;
    public Transform noteBSpawner;
    public Transform noteYSpawner;
    public Transform measureBarsSpawner;

    public TriggerNote triggerNoteA;
    public TriggerNote triggerNoteB;
    public TriggerNote triggerNoteY;


    public float triggerTime = 2.4f;
    public float triggerMargin = 0.3f;

    public float noteDestroyTime;
    private float MeasureBarDestroyTime;

    public float noteSpeed = 100f;

    public bool showTriggerZones = false;

    public delegate void NoteHitEvent(Notes note);
    public event NoteHitEvent SuccesfulNoteHit;
    public event NoteHitEvent UnsuccesfulNoteHit;
    public event NoteHitEvent NoteMissed;
    public event NoteHitEvent MeasureEndReached;

    float now;

    private void Awake()
    {
        MeasureBarDestroyTime = triggerTime;
    }

    void Update()
    {
        now = Time.time;

        foreach (Transform child in transform) {
            child.position = child.position - new Vector3(noteSpeed, 0, 0) * Time.deltaTime;
            NoteChild currentNoteChild = child.GetComponent<NoteChild>();
            float age = now - currentNoteChild.spawnTime;
            if(age > currentNoteChild.destroyTime)
            {
                if(currentNoteChild.note == Notes.MeasureBar)
                {
                    if(MeasureEndReached != null)
                    {
                        MeasureEndReached(currentNoteChild.note);
                    }
                }
                else
                {
                    if(NoteMissed != null)
                    {
                        NoteMissed(currentNoteChild.note);
                    }
                }

                Destroy(child.gameObject);          
            }
        }
    }

    public void addNote(Notes note) {
        Vector3 spawnPosition;
        NoteChild noteObject;
        switch(note)
        {
            case Notes.A:
                spawnPosition = noteASpawner.transform.position;
                noteObject = noteAPrefab;
                break;
            case Notes.B:
                spawnPosition = noteBSpawner.transform.position;
                noteObject = noteBPrefab;
                break;
            case Notes.Y:
                spawnPosition = noteYSpawner.transform.position;
                noteObject = noteYPrefab;
                break;
            case Notes.MeasureBar:
                spawnPosition = measureBarsSpawner.transform.position;
                noteObject = measureBarPrefab;
                break;
            default:
                spawnPosition = noteBSpawner.transform.position;
                noteObject = noteBPrefab;
                break;
        }

        //print("spawning note: " + note.ToString() + " at time: " + Time.timeSinceLevelLoad);

        NoteChild spawnedNote = Instantiate(noteObject, spawnPosition, Quaternion.identity, transform);

        spawnedNote.spawnTime = now;
        spawnedNote.note = note;
        spawnedNote.destroyTime = note == Notes.MeasureBar ? MeasureBarDestroyTime : noteDestroyTime;

        if(showTriggerZones)
        {
            spawnedNote.Invoke("DuplicateSelf" , triggerTime - triggerMargin);
            spawnedNote.Invoke("DuplicateSelf", triggerTime + triggerMargin);
            spawnedNote.Invoke("DuplicateSelf", triggerTime);
        }

    }

    public void checkNotes(Notes note) {
        float age;

        bool noteHasBeenHit = false; //whether a note is correctly hit this buttonpress
        NoteChild hittedNote = null;

        foreach (Transform child in transform)
        {
            NoteChild currentNote = child.GetComponent<NoteChild>();

            if(currentNote.note != note)
            {
                continue;
            }

            age = now - currentNote.spawnTime;

            if (age < triggerTime - triggerMargin) {
                //Debug.Log("TOO EARLY! age: " + age + " minTriggerAge" + minTriggerAge); // todo: remove
            } else if (age > triggerTime + triggerMargin) {
                //Debug.Log("TOO LATE age: " + age + " maxTriggerAge" + maxTriggerAge); // todo: remove
            } else {
                //Debug.Log("POINTS SCORED!: " + note.ToString());
                noteHasBeenHit = true;
                hittedNote = currentNote;
            }
        }

        if(noteHasBeenHit)
        {
            if(SuccesfulNoteHit != null)
            {
                SuccesfulNoteHit(note);
                ShowTriggerNoteEffect(note);
            }
            Destroy(hittedNote.gameObject);
        }
        else
        {
            if(UnsuccesfulNoteHit != null)
            {
                UnsuccesfulNoteHit(note);
            }

        }
    }

    public void ShowTriggerNoteEffect(Notes note)
    {
        switch(note)
        {
            case Notes.A:
                triggerNoteA.StartCoroutine(triggerNoteA.ActivateEffect());
                break;
            case Notes.B:
                triggerNoteB.StartCoroutine(triggerNoteB.ActivateEffect());
                break;
            case Notes.Y:
                triggerNoteY.StartCoroutine(triggerNoteY.ActivateEffect());
                break;
            default:
                break;
        }
    }



}
