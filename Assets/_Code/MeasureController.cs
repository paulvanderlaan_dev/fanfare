﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MeasureController : MonoBehaviour
{

    public int measuresPerCollection = 4;
    private int currentMeasureId = 0;

    public NoteSpawner noteSpawner;

    public Image CurrentMeasureStatusPanel;


    public AudioSource measureSuccesAudioSource;

    public int succesfulMeasuresNeededPerCollection = 2;

    private int currentMusicTrack = 0; //current NPC id to be unlocked or not

    int succesfulMeasures = 0;
    int failedMeasures = 0;

    bool currentMeasureSuccesful = true;

    public delegate void MeasureCollectionEvent(int Level, bool isSuccesful);
    public static event MeasureCollectionEvent MeasureCollectionEnded;

    public delegate void MeasureEvent(int measureId, bool isSuccesful);
    public static event MeasureEvent OnMeasureEnd;

    private void OnEnable()
    {
        noteSpawner.UnsuccesfulNoteHit += FailCurrentMeasure;
        noteSpawner.NoteMissed += FailCurrentMeasure;
        noteSpawner.MeasureEndReached += EndMeasureCheck;
    }

    private void OnDisable()
    {
        noteSpawner.UnsuccesfulNoteHit -= FailCurrentMeasure;
        noteSpawner.NoteMissed -= FailCurrentMeasure;
        noteSpawner.MeasureEndReached -= EndMeasureCheck;
    }

    public void FailCurrentMeasure(Notes note)
    {
        currentMeasureSuccesful = false;
        CurrentMeasureStatusPanel.color = Color.red;
    }

    public void EndMeasureCheck(Notes note)
    {
        CurrentMeasureStatusPanel.color = Color.green;
        
        if(currentMeasureSuccesful)
        {            
            succesfulMeasures++;
            measureSuccesAudioSource.Play();
            OnMeasureEnd(currentMeasureId, true);
        }
        else
        {
            failedMeasures++;
            OnMeasureEnd(currentMeasureId, false);
        }
        currentMeasureSuccesful = true;
        currentMeasureId++;
        EndMeasureCollectionCheck();
    }

    public void EndMeasureCollectionCheck()
    {
        if(currentMeasureId >= measuresPerCollection)
        {
            if(succesfulMeasures >= succesfulMeasuresNeededPerCollection)
            {
                MeasureCollectionEnded(currentMusicTrack, true);
            }
            else
            {
                MeasureCollectionEnded(currentMusicTrack, false);
            }
            currentMusicTrack++;
            succesfulMeasures = 0;
            failedMeasures = 0;
            currentMeasureId = 0;
        }
    }



    
}
