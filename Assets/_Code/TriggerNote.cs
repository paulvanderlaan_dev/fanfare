﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TriggerNote : MonoBehaviour
{
    public Image image;
    public Image activateEffectImage;

    public string buttonName;

    private Color defaultColor;
    public Color activatedColor;

    NoteSpawner noteSpawner;

    public float effectDuration = 1f;

    private void Awake()
    {
        defaultColor = image.color;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown(buttonName))
        {
            image.color = activatedColor;
        }

        if(Input.GetButtonUp(buttonName))
        {
            image.color = defaultColor;
        }
    }

    public IEnumerator ActivateEffect()
    {
        Color startColor = new Color(1f, 1f, 1f, 0f);
        Color endColor = new Color(1f, 1f, 1f, 1f);

        activateEffectImage.color = startColor;

        float t = 0f;
        float halfDuration = effectDuration / 2;

        while(t < halfDuration)
        {
            t += Time.deltaTime;
            float normalizedTime = t / effectDuration;
            activateEffectImage.color = Color.Lerp(startColor, endColor, normalizedTime);
            yield return null;
        }

        while(t < effectDuration)
        {
            t += Time.deltaTime;
            float normalizedTime = t / effectDuration;
            activateEffectImage.color = Color.Lerp(endColor, startColor, normalizedTime);
            yield return null;
        }
    }
}
