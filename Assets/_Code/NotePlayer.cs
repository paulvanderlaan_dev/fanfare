﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;

public class NotePlayer : MonoBehaviour
{
    public TimelineAsset timelineAsset;
    public NoteSpawner noteSpawner;

    public int YtrackId;
    public int BtrackId;
    public int AtrackId;
    public int measureBartrackId;

    public float timeOffset;

    private IEnumerable<IMarker> yMarkers;
    private IEnumerable<IMarker> bMarkers;
    private IEnumerable<IMarker> aMarkers;
    private IEnumerable<IMarker> measureBarMarkers;

    private void Awake()
    {
        InvokeNotesSingleTrack(YtrackId, Notes.Y);
        InvokeNotesSingleTrack(BtrackId, Notes.B);
        InvokeNotesSingleTrack(AtrackId, Notes.A);
        InvokeNotesSingleTrack(measureBartrackId, Notes.MeasureBar);
    }

    void InvokeNotesSingleTrack(int trackId, Notes noteType)
    {
        var markers = timelineAsset.GetOutputTrack(trackId).GetMarkers();
        foreach(var marker in markers)
        {
            StartCoroutine(PlayDelayedNote(noteType, (float)marker.time + timeOffset));
        }
    }

    IEnumerator PlayDelayedNote(Notes noteType, float delayTime)
    {
        yield return new WaitForSeconds(delayTime);
        noteSpawner.addNote(noteType);
    }

}
